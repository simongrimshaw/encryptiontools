package EncryptionTools;


import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;


/**
 * Utility class to construct 128-bit AES key specifications.
 *
 * To construct a key spec supply:
 *      - a password (as a char[])
 *      - an 8 byte salt (to prevent dictionary attacks)
 *
 * To perform encryption/decryption initialize a cipher with:
 *      - a secret key spec
 *      - 16-byte initialization vector
 *
 *      @author simong
 */
public final class AESUtility {

    /** Advanced Encryption Standard (AES) */
    private static final String AES = "AES";
    /** Key derivation function */
    private static final String PBKDF2_WITH_HMACSHA256 = "PBKDF2WithHmacSHA256";
    /** Key derivation iteration count */
    private static final int ITERATION_COUNT = 1024; //TODO change iterations
    /** 128 bit key length */
    private static final int AES_KEY_LENGTH = 128;
    /** SHA-1 pseudo random number generator */
    private static final String SHA1PRNG = "SHA1PRNG";
    /** Salt is 8-bytes */
    private static final int SALT_LENGTH = 8;
    /** Initialization vector is 16-bytes */
    private static final int IV_LENGTH = 16;
    /** algorithm/mode/padding */
    private static final String AES_CBC_PKCS5 = "AES/CBC/PKCS5Padding";

    /**
     * private constructor overrides default public constructor
     */
    private AESUtility(){
        //NOTHING
    }

    /**
     * Encrypt a given plaintext string using AES-128
     *
     * @param plaintext Plaintext to encrypt
     * @param keySpec Key spec for a secret key (password, salt, algorithm, key length)
     * @param iv 16-byte initialization vector
     * @return generated ciphertext as String
     *
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidAlgorithmParameterException
     * @throws InvalidKeyException
     * @throws UnsupportedEncodingException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public static byte[] encrypt(byte[] plaintext, SecretKeySpec keySpec, byte[] iv) throws
            NoSuchPaddingException,
            NoSuchAlgorithmException,
            InvalidAlgorithmParameterException,
            InvalidKeyException, UnsupportedEncodingException,
            BadPaddingException,
            IllegalBlockSizeException
    {
        Cipher cipher = Cipher.getInstance(AES_CBC_PKCS5);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(iv));
        return cipher.doFinal(plaintext);

    }

    /**
     * Decrypt a given ciphertext string using AES-128
     *
     * @param keySpec Key spec for a secret key (password, salt, algorithm, key length)
     * @param iv 16-byte initialization vector
     * @return generated ciphertext as String
     * @return
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidAlgorithmParameterException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public static byte[] decrypt(byte[] ciphertext, SecretKeySpec keySpec, byte[] iv) throws
            NoSuchPaddingException,
            NoSuchAlgorithmException,
            InvalidAlgorithmParameterException,
            InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance(AES_CBC_PKCS5);
        cipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(iv));
        return cipher.doFinal(ciphertext);
    }

    /**
     * Generate a 128-bit AES key spec from a supplied password and salt
     *
     * @param password password as char array //TODO is there a limit to length?
     * @param salt 8-byte salt prevents dictionary attacks
     * @return AES key
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static SecretKeySpec generateKey(char[] password, byte[] salt) throws
            NoSuchAlgorithmException,
            InvalidKeySpecException
    {
        SecretKeyFactory factory = SecretKeyFactory.getInstance(PBKDF2_WITH_HMACSHA256);
        KeySpec spec = new PBEKeySpec(password, salt, ITERATION_COUNT, AES_KEY_LENGTH);
        SecretKey tmp = factory.generateSecret(spec);

        return new SecretKeySpec(tmp.getEncoded(), AES);
    }

    /**
     * Spawn a 16 byte initialization vector
     * @return Generated IV
     * @throws NoSuchAlgorithmException
     */
    public static byte[] spawnIV() throws NoSuchAlgorithmException {
        SecureRandom sha1Prng = SecureRandom.getInstance("SHA1PRNG");
        byte[] iv = new byte[IV_LENGTH];
        sha1Prng.nextBytes(iv);
        return iv;
    }

    /**
     * Spawn an 8 byte salt
     * @return Generated salt
     * @throws NoSuchAlgorithmException
     */
    public static byte[] spawnSalt() throws NoSuchAlgorithmException {
        SecureRandom sha1Prng = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[SALT_LENGTH];
        sha1Prng.nextBytes(salt);
        return salt;
    }

}
