package EncryptionTools;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;

/**
 * Models a 4096-bit RSA Key Pair
 */
public final class RSAUtility {

    /** The RSA asymmetric key algorithm */
    private static final String RSA = "RSA";

    /** 4096-bits is the highest key strength available for RSA */
    private static int KEY_STRENGTH = 4096;

    /**
     * Private constructor overrides default public constructor
     */
    private RSAUtility(){
        //NOTHING
    }

    /**
     * Generate a 4096-bit RSA key pair
     *
     * @return 4096-bit RSA key pair
     * @throws NoSuchAlgorithmException [should never be triggered] "RSA" algorithm not recognised
     */
    public static KeyPair generateNewKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator rsaKeyPairGenerator = KeyPairGenerator.getInstance(RSA);
        rsaKeyPairGenerator.initialize(KEY_STRENGTH);
        return rsaKeyPairGenerator.generateKeyPair();
    }

    /**
     * Extract the private key from a public/private key keypair
     * @param keyPair input keypair
     * @return private key
     */
    public static PrivateKey extractPrivateKeyFromKeyPair(KeyPair keyPair){
        return keyPair.getPrivate();
    }

    /**
     * Extract the public key from a public/private key keypair
     * @param keypair input keypair
     * @return public key
     */
    public static PublicKey extractPublicKeyFromKeyPair(KeyPair keypair){
        return keypair.getPublic();
    }

    /**
     * Encrypt plaintext using a supplied RSA key
     *
     * @param key key to encrypt with (public or private)
     * @param plaintext plaintext to encrypt
     * @return encrypted ciphertext
     *
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public static byte[] encrypt(Key key, byte[] plaintext) throws
            NoSuchPaddingException,
            NoSuchAlgorithmException,
            InvalidKeyException,
            BadPaddingException,
            IllegalBlockSizeException
    {
        final Cipher cipher = Cipher.getInstance(RSA);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] ciphertext = cipher.doFinal(plaintext);

        NoStrings.nukeArray(plaintext);     //NUKE IT!!!

        return ciphertext;
    }

    /**
     * Decrypt ciphertext using a supplied RSA key
     *
     * @param key key to decrypt with (public or private)
     * @param ciphertext ciphertext to decrypt
     * @return decrypted plaintext
     *
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public static byte[] decrypt(Key key, byte[] ciphertext) throws
            NoSuchPaddingException,
            NoSuchAlgorithmException,
            InvalidKeyException,
            BadPaddingException,
            IllegalBlockSizeException
    {
        final Cipher cipher = Cipher.getInstance(RSA);
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] plaintext = cipher.doFinal(ciphertext);

        NoStrings.nukeArray(ciphertext);     //NUKE IT!!!

        return plaintext;
    }

}
