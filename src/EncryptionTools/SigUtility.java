package EncryptionTools;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

/**
 * Created by Lumpsucker on 26/05/2017.
 */
public final class SigUtility {

    private static final String SHA_1 = "SHA1";

    /**
     * Override default public constructor
     */
    private SigUtility(){
        //NOTHING
    }

    /**
     * Generate a digital signature:
     *      -Perform a one-way hash function on the input
     *      -'sign' the digest by encrypting with the user's private key
     * @param message message to sign
     * @param timestamp message timestamp
     * @param keyStore key store containing the user's private key
     * @return Digital signature
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws InvalidAlgorithmParameterException
     * @throws InvalidKeySpecException
     */
    public static byte[] sign(byte[] message, byte[] timestamp, PrivateKeyStore keyStore) throws
            NoSuchAlgorithmException,
            NoSuchPaddingException,
            InvalidKeyException,
            IllegalBlockSizeException,
            BadPaddingException,
            InvalidAlgorithmParameterException,
            InvalidKeySpecException, IOException {

        byte[] input = concatStampAndPayload(timestamp, message);
        MessageDigest md = MessageDigest.getInstance(SHA_1);
        byte[] hashBytes = md.digest(input);
        return keyStore.encryptWithPrivateKey(hashBytes);
    }

    /**
     * Decrypt the signature using the signer's public key to reveal a SHA-1 message digest
     *
     * @param signature digital signature in byte form
     * @param key The signer's public key
     * @return decrypted message digest
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public static byte[] decryptSignature(byte[] signature, PublicKey key) throws
            IllegalBlockSizeException,
            InvalidKeyException,
            BadPaddingException,
            NoSuchAlgorithmException,
            NoSuchPaddingException
    {
        return RSAUtility.decrypt(key, signature);
    }

    /**
     * Compare two digital signatures.
     * RSA will produce different output if the same key is used to encrypt
     * the same input multiple times (this is due to the random generation of
     * padding).
     *
     * THUS: we have to decrypt both signatures and retrieve the message digests
     * to do a comparison.  Unlike RSA, the SHA-1 hash function will always produce
     * the same output when supplied with the same input.
     *
     * @param sig1 Signature to compare
     * @param sig2 Other signature to compare
     * @param key Public key that complements the private RSA key used to encrypt the digests initally
     * @return [TRUE/FALSE] Do the signatures match?
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws NoSuchPaddingException
     */
    public static boolean compareSignatures(byte[] sig1, byte[] sig2, PublicKey key) throws InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException {
        byte[] digest1 = decryptSignature(sig1, key);
        byte[] digest2 = decryptSignature(sig2, key);

        //return digest1.equals(digest2); <--arrays are object type not primitive, hence this fails (see line below)
        return Arrays.equals(digest1, digest2);
    }

    /**
     * Concatenate the timestamp and payload together ready to be processed into a
     * digital signature.
     *
     * @param stamp timestamp as bytes
     * @param payload message payload as bytes
     * @return concatenated byte array
     * @throws IOException
     */
    private static byte[] concatStampAndPayload(byte[] stamp, byte[] payload) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        outputStream.write( stamp );
        outputStream.write( payload );

        byte[] output =  outputStream.toByteArray( );

        NoStrings.nukeArray(stamp);  // NUKE IT
        NoStrings.nukeArray(payload);  // NUKE IT

        outputStream.reset();       // Resets the count field of this byte array output
        // stream to zero, so that all currently accumulated
        // output in the output stream is discarded.

        return output;
    }
}
