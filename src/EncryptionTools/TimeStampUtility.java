package EncryptionTools;

import java.nio.ByteBuffer;
import java.sql.Timestamp;

/**
 * Created by Lumpsucker on 26/05/2017.
 */
public class TimestampUtility {


    private TimestampUtility(){
        //NOTHING
    }

    /**
     * Get the time in milliseconds since 01/01/1970 00:00:00:000
     * @return long representing time in milliseconds
     */
    public static long getStamp(){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return timestamp.getTime();
    }

    /**
     * Get the time in millisecond since 01/01/1970 00:00:00:000
     * as a byte array
     * Used in signing encrypted messages.  The time in bytes is prepended
     * to the payload message as bytes before cryptographic conversion to a
     * digital signature using a hashing algorithm and encryption with a
     * private key.
     *
     * @return byte[] represeting time in milliseconds
     */
    public static byte[] getStampAsBytes(){
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(getStamp());
        return buffer.array();
    }

    /**
     * Convert a byte array representing a timestamp to an actual timestamp
     * (usually recovered from the signature of an encrypted message)
     * @param timeInBytes timestamp as a byte[]
     * @return timestamp
     */
    public Timestamp reconstituteStamp(byte[] timeInBytes){
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.put(timeInBytes);
        buffer.flip();//need flip
        Long timeAsLong = buffer.getLong();
        return new Timestamp(timeAsLong);
    }

    /**
     * Compare two timestamp objects
     *
     * @param ts1 a timestamp
     * @param ts2 another timstamp
     * @return [TRUE/FALSE] do the timestamps match?
     */
    public boolean compareStamps(Timestamp ts1, Timestamp ts2){
        if(ts1.equals(ts2)){
            return true;
        }
        return false;
    }
}
