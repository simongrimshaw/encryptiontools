package EncryptionTools;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.DestroyFailedException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;


/**
 * Created by Lumpsucker on 23/05/2017.
 */
public class PrivateKeyStore implements Serializable {
    /** Failed to destroy key warning message */
    private static final String FAILED_DESTROY_WARNING = "WARNING: failed to destroy private key";
    /** RSA asymmetric key algorithm */
    private static final String RSA = "RSA";

    private byte[] encryptedPrivateKey;
    private SecretKeySpec keySpec;
    private byte[] iv;
    private String primaryEncodingFormat;
    private transient char[] userPassword;  //this should NEVER exist anywhere, but in memory
                                            //ideally it would never be stored full stop, but
                                            //I can't force the user to type it in every time
                                            //an encrypted message arrives etc...



    public PrivateKeyStore(char[] password, PrivateKey privateKey) throws
            NoSuchPaddingException,
            BadPaddingException,
            InvalidAlgorithmParameterException,
            NoSuchAlgorithmException,
            IllegalBlockSizeException,
            UnsupportedEncodingException,
            InvalidKeyException,
            InvalidKeySpecException
    {
        //store user password for future decryption
        userPassword = password;
        primaryEncodingFormat = privateKey.getFormat();
        encryptKey(password, privateKey);
    }

    /**
     * Update the user's password:
     *  -Store new password
     *  -Decrypt the existing password
     *  -Encrypt the existing key with the new password
     *  -Store the newly encrypted key
     *
     * @param newPassword new password
     * @throws NoSuchPaddingException
     * @throws BadPaddingException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws UnsupportedEncodingException
     * @throws InvalidKeyException
     * @throws InvalidKeySpecException
     */
    public void updateUserPassword(char[] newPassword) throws
            NoSuchPaddingException, BadPaddingException,
            InvalidAlgorithmParameterException,
            NoSuchAlgorithmException,
            IllegalBlockSizeException,
            UnsupportedEncodingException,
            InvalidKeyException,
            InvalidKeySpecException
    {
        PrivateKey currentKey = decryptAndRetrieveKey();
        encryptKey(newPassword, currentKey);
        userPassword = newPassword;
        nukeItFromOrbit(currentKey);    //NUKE IT!!!
    }

    /**
     * Decrypt stored key and use it to decrypt supplied ciphertext
     *
     * @param ciphertext ciphertext to decrypt
     * @return generated plaintext
     *
     * @throws NoSuchPaddingException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws InvalidKeyException
     * @throws InvalidKeySpecException
     */
    public byte[] decryptWithPrivateKey (byte[] ciphertext) throws
            NoSuchPaddingException,
            InvalidAlgorithmParameterException,
            NoSuchAlgorithmException,
            IllegalBlockSizeException,
            BadPaddingException,
            InvalidKeyException,
            InvalidKeySpecException
    {
        Key privateKey = decryptAndRetrieveKey();
        byte[] plaintext = RSAUtility.decrypt(privateKey, ciphertext);
        NoStrings.nukeArray(ciphertext);
        return plaintext;
    }

    /**
     * Decrypt stored key and use it to encrypt supplied plaintext
     *
     * @param plaintext plaintext to encrypt
     * @return generated ciphertext
     * @throws NoSuchPaddingException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws InvalidKeyException
     * @throws InvalidKeySpecException
     */
    public byte[] encryptWithPrivateKey (byte[] plaintext) throws
            NoSuchPaddingException,
            InvalidAlgorithmParameterException,
            NoSuchAlgorithmException,
            IllegalBlockSizeException,
            BadPaddingException,
            InvalidKeyException,
            InvalidKeySpecException
    {
            Key privateKey = decryptAndRetrieveKey();
            byte[] ciphertext = RSAUtility.encrypt(privateKey, plaintext);
            NoStrings.nukeArray(plaintext);
            return ciphertext;
    }

    private PrivateKey decryptAndRetrieveKey() throws
            NoSuchPaddingException,
            InvalidKeyException,
            NoSuchAlgorithmException,
            IllegalBlockSizeException,
            BadPaddingException,
            InvalidAlgorithmParameterException,
            InvalidKeySpecException
    {
        byte[]  decryptedKeyBytes = AESUtility.decrypt(encryptedPrivateKey, keySpec, iv);
        KeyFactory factory = KeyFactory.getInstance(RSA);
        PrivateKey privateKey = factory.generatePrivate(new PKCS8EncodedKeySpec(decryptedKeyBytes));

        NoStrings.nukeArray(decryptedKeyBytes);     //NUKE IT!!

        return privateKey;
    }

    private void encryptKey(char[] password, PrivateKey privateKey) throws
            NoSuchAlgorithmException,
            InvalidKeySpecException,
            NoSuchPaddingException,
            InvalidKeyException,
            UnsupportedEncodingException,
            IllegalBlockSizeException,
            BadPaddingException,
            InvalidAlgorithmParameterException
    {
        //generate AES key
        byte[] salt = AESUtility.spawnSalt();
        iv = AESUtility.spawnIV();
        keySpec = AESUtility.generateKey(password, salt);
        //encrypt RSA private key w/ AES key and store
        byte[] plaintextKey = privateKey.getEncoded();
        encryptedPrivateKey = AESUtility.encrypt((plaintextKey), keySpec, iv);
        //destroy RSA private key
        nukeItFromOrbit(privateKey);    //NUKE IT!!!
    }

    private boolean nukeItFromOrbit(PrivateKey privateKey) {
        int nukeAttempts = 1;
        Boolean nuked = false;

        while (nuked && nukeAttempts<101) {
            try {
                /*  PrivateKey.destroy():
                 *  -------------------------------------------------------------------------
                 *  Destroy this Object.
                 *  Sensitive information associated with this Object is destroyed or cleared.
                 *  Subsequent calls to certain methods on this Object will result in an
                 *  IllegalStateException being thrown.
                 */
                privateKey.destroy();
                nuked = true;
                System.out.println("Key destroyed after " +nukeAttempts+ " attempts");
            } catch (DestroyFailedException d) {
                nukeAttempts++;
            }
        }
        if(nuked == false){
            System.out.println(FAILED_DESTROY_WARNING);
        }
        return privateKey.isDestroyed();
    }

}
