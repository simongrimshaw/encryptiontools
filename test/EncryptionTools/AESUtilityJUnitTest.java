package EncryptionTools;

import org.junit.jupiter.api.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.junit.MatcherAssert.assertThat;

/**
 * Tests for AES Encrypter utility class
 *
 * @author simong
 */

public class AESUtilityJUnitTest {

    /** Advanced Encryption Standard (AES) algorithm */
    private static final String AES = "AES";

    @Test
    public void test_Encrypted_Plaintext_Can_Be_Decrypted_From_Generated_Ciphertext() throws
            NoSuchAlgorithmException,
            InvalidKeySpecException,
            NoSuchPaddingException,
            InvalidKeyException,
            UnsupportedEncodingException,
            IllegalBlockSizeException,
            BadPaddingException,
            InvalidAlgorithmParameterException
    {

        //Given
        char[] plaintext = {'p','l','a','i','n','t','e','x','t'};
        SecretKeySpec keySpec = givenKeySpecFromPasswordAndSalt();
        byte[] iv = AESUtility.spawnIV();

        //When
        char[] result = NoStrings.toChars(encryptThenDecryptPlaintextUsingKey(plaintext, keySpec, iv));
        //duplicated as plaintext gets wiped (probably by nuke method in NoString)
        char[] expected = {'p','l','a','i','n','t','e','x','t'};

        //Then
        assertThat(result,is(expected));
    }

    @Test
    public void key_Length_Is_128_Bits() throws
            NoSuchAlgorithmException,
            InvalidKeySpecException
    {
        //Given
        SecretKey key = givenKeySpecFromPasswordAndSalt();

        //When
        int keyLength = key.getEncoded().length * 8; //16 bytes = 128 bits

        //Then
        assertThat(keyLength, is(128));
    }

    @Test
    public void key_Algorithm_Is_AES() throws
            InvalidKeySpecException,
            NoSuchAlgorithmException
    {
        //Given
        SecretKeySpec keySpec = givenKeySpecFromPasswordAndSalt();

        //When
        String result = keySpec.getAlgorithm();

        //Then
        assertThat(result, is(AES));
    }

    @Test
    public void iv_Is_16_Bytes() throws NoSuchAlgorithmException {
        byte[] iv = AESUtility.spawnIV();
        assertThat(iv.length,is(16));
    }

    /**
     * Confirm padding is behaving as expected
     */
    @Test
    public void encrypt_Output_Length_Is_Multiple_Of_16() throws
            NoSuchAlgorithmException,
            InvalidKeySpecException,
            NoSuchPaddingException,
            InvalidKeyException,
            UnsupportedEncodingException,
            IllegalBlockSizeException,
            BadPaddingException,
            InvalidAlgorithmParameterException
    {
        char[] plaintext = {'p','l','a','i','n','t','e','x','t'};
        byte[] iv = AESUtility.spawnIV();
        SecretKeySpec keySpec = givenKeySpecFromPasswordAndSalt();
        byte[] output = AESUtility.encrypt(NoStrings.toBytes(plaintext),keySpec, iv);

        assertThat(output.length%16, is(0));
    }

    private SecretKeySpec givenKeySpecFromPasswordAndSalt() throws NoSuchAlgorithmException, InvalidKeySpecException {
        char[] password = {'s','3','c','u','r','3'};
        byte[] salt = AESUtility.spawnSalt();
        return AESUtility.generateKey(password, salt);
    }

    private byte[] encryptThenDecryptPlaintextUsingKey(char[] plaintext, SecretKeySpec keySpec, byte[] iv) throws
            NoSuchPaddingException,
            BadPaddingException,
            InvalidKeyException,
            NoSuchAlgorithmException,
            IllegalBlockSizeException,
            UnsupportedEncodingException,
            InvalidAlgorithmParameterException
    {

        byte[] ciphertext = AESUtility.encrypt(NoStrings.toBytes(plaintext), keySpec, iv);         //encrypt...
        return AESUtility.decrypt(ciphertext, keySpec, iv);  //...and decrypt
    }
}