package EncryptionTools;

import org.junit.jupiter.api.Test;
import java.io.UnsupportedEncodingException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by Lumpsucker on 24/05/2017.
 */
public class NoStringsJUnitTest {

    /** Char array for testing */
    private static final char[] EXPECTED_RESULT = {'t','h','e',' ','o','n','l','y',' ','t','i','m','e',' ','I','\'','m',' ','g','o','i','n','g',' ','t','o',' ','u','s','e',' ','a',' ','s','t','r','i','n','g','.','.','.'};

    @Test
    public void test_Conversion_Chars_To_Bytes_To_Chars(){
        char[] input = {'t','h','e',' ','o','n','l','y',' ','t','i','m','e',' ','I','\'','m',' ','g','o','i','n','g',' ','t','o',' ','u','s','e',' ','a',' ','s','t','r','i','n','g','.','.','.'};
        byte[] bytes = NoStrings.toBytes(input);
        char[] result = NoStrings.toChars(bytes);

       assertThat(result, is(EXPECTED_RESULT));
    }

    @Test
    public void test_Conversion_Bytes_To_Chars_To_Bytes() throws UnsupportedEncodingException {

        byte[] input = NoStrings.stripBOMs("I promise this is the last string...".getBytes("UTF-16"));
        //duplicated as inputBytes gets nuked at some point
        byte[] expected = NoStrings.stripBOMs("I promise this is the last string...".getBytes("UTF-16"));
        input = NoStrings.stripBOMs(input);
        char[] chars = NoStrings.toChars(input);
        byte[] result = NoStrings.toBytes(chars);

        assertThat(result, is(expected));
    }

    @Test
    public void test_Nuke_Char_Array(){
        char[] charArray = {'t','h','e',' ','o','n','l','y',' ','t','i','m','e',' ','I','\'','m',' ','g','o','i','n','g',' ','t','o',' ','u','s','e',' ','a',' ','s','t','r','i','n','g','.','.','.'};;
        NoStrings.nukeArray(charArray);

        for(int i = 0; i < charArray.length; i++ ){
            assertThat(charArray[i], is('\0'));
        }
    }

    @Test
    public void test_Nuke_Byte_Array(){
        char[] input = {'t','h','e',' ','o','n','l','y',' ','t','i','m','e',' ','I','\'','m',' ','g','o','i','n','g',' ','t','o',' ','u','s','e',' ','a',' ','s','t','r','i','n','g','.','.','.'};;
        byte[] byteArray = NoStrings.toBytes(input);
        NoStrings.nukeArray(byteArray);
        byte zeroByte = 0;
        for(int i = 0; i < byteArray.length; i++ ){
            assertThat(byteArray[i], is(zeroByte));
        }
    }
}
