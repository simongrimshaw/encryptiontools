package EncryptionTools;

import org.junit.jupiter.api.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.junit.MatcherAssert.assertThat;

/**
 * Created by Lumpsucker on 26/05/2017.
 */
public class SigUtilityJUnitTest {
    static final char[] PASSWORD = {'p','a','s','s','w','o','r','d'};
    static final char[] PLAINTEXT = {'p','l','a','i','n','t','e','x','t'};

    @Test
    public void generate_And_Compare_Signatures_Where_Signatures_Match() throws
            NoSuchPaddingException,
            IOException,
            InvalidKeyException,
            NoSuchAlgorithmException,
            IllegalBlockSizeException,
            BadPaddingException,
            InvalidAlgorithmParameterException,
            InvalidKeySpecException {

        KeyPair rsaKeyPair = givenRSAKeyPair();
        PrivateKeyStore keyStore = new PrivateKeyStore(PASSWORD, rsaKeyPair.getPrivate());
        byte[] signature1 = generateSignature(keyStore);
        //signature 2 must be an exact clone of signature 1 to pass.  Due to the inclusion of a timestamp
        //this must be done by copying the bytes
        byte[] signature2 = new byte[signature1.length];
        for(int i = 0; i < signature1.length; i++){
            signature2[i] = signature1[i];
        }
        Boolean matchingSignatures = SigUtility.compareSignatures(signature1, signature2, rsaKeyPair.getPublic());
        assertThat(matchingSignatures, is(true));
    }

    @Test
    public void generate_And_Compare_Signatures_Where_Signatures_Dont_Match() throws
            NoSuchPaddingException,
            IOException,
            InvalidKeyException,
            NoSuchAlgorithmException,
            IllegalBlockSizeException,
            BadPaddingException,
            InvalidAlgorithmParameterException,
            InvalidKeySpecException {
        KeyPair rsaKeyPair = givenRSAKeyPair();
        PrivateKeyStore keyStore = new PrivateKeyStore(PASSWORD, rsaKeyPair.getPrivate());
        //timestamp ensures data integrity hence the signatures below will not matchK
        byte[] signature1 = generateSignature(keyStore);
        byte[] signature2 = generateSignature(keyStore);

        Boolean matchingSignatures = SigUtility.compareSignatures(signature1, signature2, rsaKeyPair.getPublic());
        assertThat(matchingSignatures, is(true));
    }

    private KeyPair givenRSAKeyPair() throws NoSuchAlgorithmException {

        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        return keyGen.generateKeyPair();
    }

    private byte[] generateSignature(PrivateKeyStore keyStore) throws
            NoSuchPaddingException,
            InvalidAlgorithmParameterException,
            NoSuchAlgorithmException,
            IllegalBlockSizeException,
            IOException,
            BadPaddingException,
            InvalidKeyException,
            InvalidKeySpecException {

        byte[] timestamp = TimestampUtility.getStampAsBytes();
        return SigUtility.sign(NoStrings.toBytes(PLAINTEXT), timestamp, keyStore);
    }
}
