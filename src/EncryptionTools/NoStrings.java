package EncryptionTools;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * Utility class centred around providing tools to AVOID USING STRINGS.
 * Strings are immutable, char/byte arrays can be overwritten,
 * thus Strings are a poor choice for storing information such as pass-
 * words.
 *
 * String literals e.g. String a = "wiggins" are interned in the 'String
 * Constant pool' on the heap and will not be garbage collected.
 *
 * A String created using String a = new String("wiggins") is at least
 * eligible for garbage collections, however as stated above neither are
 * a good idea.
 *
 * SO, you should BE LIKE PINOCCHIO:
 *
 *  [Pinocchio]: I've got no strings
 *  To hold me down
 *  To make me fret, or make me frown
 *  I had strings
 *  But now I'm free
 *  There are no strings on me
 *
 *  A NOTE ON NUKING ARRAYS:
 *
 *  Primitive types such as char & byte are stored on a stack and are
 *  freed from memory as soon as a method returns.
 *
 *  Objects are stored on the heap and the reference to the object is stored
 *  on the stack so the reference gets obliterated when the method returns,
 *  but the actual object will hang around waiting for garbage collection.
 *
 *  THUS: primitives are preferable for sensitive information such as pass-
 *  words, however it can't hurt to nuke them as well (assuming performance
 *  is not massively affected).
 *
 */
public final class NoStrings {

    /**
     * Override default public constructor
     */
    private NoStrings(){
        //NOTHING
    }

    /**
     * Transform a char array into a byte array (UTF_16 encoding)
     *
     * @param payload char array to transform
     * @return byte array
     */
    public static byte[] toBytes(char[] payload){

        ByteBuffer buf = StandardCharsets.UTF_16.encode(CharBuffer.wrap(payload));
        byte[] array = new byte[buf.limit()];
        buf.get(array);

       nukeArray(payload);     //NUKE IT
        byte zeroByte = 0;
        //If buffer ByteBuffer has a suitable array backing it

        if(buf.hasArray()){
            Arrays.fill(buf.array(), zeroByte); //Fill array that backs ByteBuffer with 0's
        }
        buf.clear();        //Clears this buffer. The position is set to zero, the limit is
                            // set to the capacity, and the mark is discarded.
                            //NOTE: does not clear data, simply resets the buffer pos.
        return stripBOMs(array);
    }

    /**
     * NOTE: parameter 'payload' cannot be nuked or the returned value 'array' also gets nuked.
     * This doesn't cause too much of a security issue as payload will be freed from the stack
     * anyway when the method concludes.
     *
     * I believe this is to do with the behaviour of ByteBuffer.wrap():
     *
     *          "The new buffer will be backed by the given byte array; that is,
     *          modifications to the buffer will cause the array to be modified and
     *          vice versa."
     *
     *
     * @param payload byte array to transform into a char array
     * @return char array
     */
    public static char[] toChars(byte[] payload){
        CharBuffer buf = StandardCharsets.UTF_16.decode(ByteBuffer.wrap(payload));
        char[] array = new char[buf.limit()];
        buf.get(array);
        if(buf.hasArray()){
            Arrays.fill(buf.array(), '\0');
        }
        buf.clear();
        nukeArray(payload);
        return array;
    }

    /**
     * Overwrite char array with null '\0'
     *
     * @param input array to sanitize
     */
    public static void nukeArray(char[] input){
        for(int i = 0; i < input.length; i++){
            input[i] = '\0';
        }
    }

    /**
     * Overwrite byte array with  zeroes
     *
     * @param input array to sanitize
     */
    public static void nukeArray(byte[] input){
        byte zeroByte = 0;
        for(int i = 0; i < input.length; i++){
            input[i] = zeroByte;
        }
    }

    /**
     * If a byte order mark (BOM) has been prepended to the array during conversion
     * we want to strip this out.
     *
     * This utility class uses UTF-16 encoding and the assumption is made the
     * software is being run on a Little Endian machine.  Under these circumstances the
     * BOM is recorded as [-2,-1]
     *
     * @param inputBytes byte array to inspect
     * @return byte array w/ BOM stripped
     */
    public static byte[] stripBOMs(byte[] inputBytes){

        if(inputBytes[0] == -2 && inputBytes[1] == -1 && inputBytes.length%16!=0) {
            int lengthMinusBOM = inputBytes.length - 2;
            ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();
            bytesOut.write(inputBytes, 2, lengthMinusBOM);
            byte[] outputBytes = bytesOut.toByteArray();

            nukeArray(inputBytes);  // NUKE IT

            bytesOut.reset();       // Resets the count field of this byte array output
                                    // stream to zero, so that all currently accumulated
                                    // output in the output stream is discarded.

            return outputBytes;
        }
        return inputBytes;
    }
}
