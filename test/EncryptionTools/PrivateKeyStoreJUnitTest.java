package EncryptionTools;



import org.junit.jupiter.api.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;

import java.security.*;
import java.security.spec.InvalidKeySpecException;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by Lumpsucker on 25/05/2017.
 */
public class PrivateKeyStoreJUnitTest {

    @Test
    public void encrypt_With_Stored_Key() throws
            NoSuchAlgorithmException,
            NoSuchPaddingException,
            InvalidKeyException,
            UnsupportedEncodingException,
            IllegalBlockSizeException,
            BadPaddingException,
            InvalidAlgorithmParameterException,
            InvalidKeySpecException
    {
        KeyPair keyPair = RSAUtility.generateNewKeyPair();
        PrivateKey privateKey = keyPair.getPrivate();
        char[] password = {'s','e','c','r','e','t'};
        PrivateKeyStore keyStore = new PrivateKeyStore(password, privateKey);

        char[] plaintext = {'p','l','a','i','n','t','e','x','t'};
        byte[] cipherBytes = keyStore.encryptWithPrivateKey(NoStrings.toBytes(plaintext));

        PublicKey publicKey = keyPair.getPublic();
        char[] result = NoStrings.toChars(RSAUtility.decrypt(publicKey,cipherBytes));

        char[] expectedResult = {'p','l','a','i','n','t','e','x','t'};
        assertThat(result, is(expectedResult));
    }

    @Test
    public void decrypt_With_Stored_Key() throws
            NoSuchAlgorithmException,
            IllegalBlockSizeException,
            InvalidKeyException,
            BadPaddingException,
            NoSuchPaddingException,
            InvalidAlgorithmParameterException,
            UnsupportedEncodingException,
            InvalidKeySpecException
    {
        KeyPair keyPair = RSAUtility.generateNewKeyPair();
        PublicKey publicKey = keyPair.getPublic();
        char[] plaintext = {'p','l','a','i','n','t','e','x','t'};
        byte[] cipherBytes = RSAUtility.encrypt(publicKey,NoStrings.toBytes(plaintext));

        char[] password = {'s','e','c','r','e','t'};
        PrivateKey privateKey = keyPair.getPrivate();
        PrivateKeyStore keyStore = new PrivateKeyStore(password, privateKey );
        byte[] plainBytes = keyStore.decryptWithPrivateKey(cipherBytes);

        char[] expected = {'p','l','a','i','n','t','e','x','t'};
        assertThat(NoStrings.toChars(plainBytes), is (expected));
    }

}
