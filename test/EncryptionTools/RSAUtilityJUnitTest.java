package EncryptionTools;

import org.junit.jupiter.api.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.junit.MatcherAssert.assertThat;

public class RSAUtilityJUnitTest {

    private static final int EXPECTED_RSA_KEY_STRENGTH = 4096;

    @Test
    public void key_Pair_Has_4096_Bit_Key_Strength() throws NoSuchAlgorithmException {

        //Given RSA key pair...
        KeyPair keyPair = RSAUtility.generateNewKeyPair();
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();

        //When keys are extracted and key strength inspected...
        int privateKeyStrength = privateKey.getModulus().bitLength();
        int publicKeyStrength = publicKey.getModulus().bitLength();

        //Then key strength is [4096 bits]
        assertThat(privateKeyStrength, is(EXPECTED_RSA_KEY_STRENGTH));
        assertThat(publicKeyStrength, is(EXPECTED_RSA_KEY_STRENGTH));
    }

    @Test
    public void key_Pair_Agorithm_Is_RSA() throws NoSuchAlgorithmException {

        //given RSA key pair...
        KeyPair keyPair = RSAUtility.generateNewKeyPair();
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();

        //When key algorithm is inspected...
        String algorithm = privateKey.getAlgorithm();

        //Then algorithm is RSA.
        assertThat(algorithm, is("RSA"));
    }

    @Test
    public void public_Key_Encryption_Can_Be_Decrypted_With_Private_Key() throws
            NoSuchAlgorithmException,
            InvalidKeyException,
            BadPaddingException,
            NoSuchPaddingException,
            IllegalBlockSizeException
    {
        //Given
        char[] plaintext = {'p','l','a','i','n','t','e','x','t'};
        KeyPair keyPair = RSAUtility.generateNewKeyPair();

        //When
        char[] result = NoStrings.toChars(PubKeyEncryptPrivKeyDecrypt(plaintext, keyPair));
        //duplicated as plaintext gets wiped (probably by nuke method in NoString)
        char[] expected = {'p','l','a','i','n','t','e','x','t'};

        //Then
        assertThat(result, is(expected));
    }

    @Test
    public void private_Key_Encryption_Can_Be_Decrypted_With_Public_Key() throws
            NoSuchAlgorithmException,
            InvalidKeyException,
            BadPaddingException,
            NoSuchPaddingException,
            IllegalBlockSizeException
    {
        //Given
        char[] plaintext = {'p','l','a','i','n','t','e','x','t'};
        KeyPair keyPair = RSAUtility.generateNewKeyPair();

        //When
        char[] result = NoStrings.toChars(PrivKeyEncryptPubKeyDecrypt(plaintext, keyPair));
        //duplicated as plaintext gets wiped (probably by nuke method in NoString)
        char[] expected = {'p','l','a','i','n','t','e','x','t'};

        //Then
        assertThat(result, is(expected));
    }

    private byte[] PubKeyEncryptPrivKeyDecrypt(char[] plaintext, KeyPair keypair) throws
            IllegalBlockSizeException,
            InvalidKeyException,
            BadPaddingException,
            NoSuchAlgorithmException,
            NoSuchPaddingException
    {
        byte[] plainBytes = NoStrings.toBytes(plaintext);
        byte[] cipherBytes = RSAUtility.encrypt(keypair.getPublic(), plainBytes);
        return RSAUtility.decrypt(keypair.getPrivate(), cipherBytes);
    }

    private byte[] PrivKeyEncryptPubKeyDecrypt(char[] plaintext, KeyPair keypair) throws
            IllegalBlockSizeException,
            InvalidKeyException,
            BadPaddingException,
            NoSuchAlgorithmException,
            NoSuchPaddingException
    {
        byte[] plainBytes = NoStrings.toBytes(plaintext);
        byte[] cipherBytes = RSAUtility.encrypt(keypair.getPrivate(), plainBytes);
        return RSAUtility.decrypt(keypair.getPublic(), cipherBytes);
    }
}

